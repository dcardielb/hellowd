package com.hellow.d;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloWD {
	
	
	
	public String getAddress () throws UnknownHostException {
		 return InetAddress.getLocalHost().getHostAddress();
	}
	
	@GetMapping("/hello")
	public String hello () {
		String message = null;
		try {
			message = "Hello from " + getAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;
			
	}
}
