FROM openjdk:14

RUN mkdir /home/app

COPY target/HelloWD-0.0.1-SNAPSHOT.jar /home/app

WORKDIR /home/app

EXPOSE 8080

CMD java -jar HelloWD-0.0.1-SNAPSHOT.jar
